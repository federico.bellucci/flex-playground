import React from 'react';
import Reactdom from 'react-dom';
import App from './components/App';
import './index.sass'

Reactdom.render(<App />, document.getElementById("app"))