import React, { useState } from 'react'
import DrawArea from './DrawArea'
import Controls from './Controls'
import Button from './Button'
import './App.sass'

export default () => {
  const newContainerStyles = {
    height: '',
    width: '',
    flexDirection: '',
    flexWrap: '',
    justifyContent: '',
    alignItems: '',
    alignContent: ''
  }

  const newContainerProps = {
    alignItemsSafe: { safe: false, unsafe: false },
    alignContentSafe: { safe: false, unsafe: false },
    justifyContentSafe: { safe: false, unsafe: false },
  }

  const [containers, setContainers] = useState([
    {
      styles: {
        ...newContainerStyles,
        height: '300px',
        flexWrap: 'wrap',
        alignContent: 'center'
      },
      props: {
        ...newContainerProps
      }
    },
    {
      styles: {
        ...newContainerStyles,
        flexWrap: 'wrap-reverse'
      },
      props: {
        ...newContainerProps
      }
    },
    {
      styles: {
        ...newContainerStyles,
        height: '150px',
        alignItems: 'flex-end'
      },
      props: {
        ...newContainerProps
      }
    }
  ])
  const [selected, setSelected] = useState(0)

  const addContainer = () => {
    setContainers([
      ...containers,
      {
        styles: { ...newContainerStyles },
        props: { ...newContainerProps }
      }
    ])
  }

  const removeContainer = index => {
    setContainers([...containers.slice(0, index), ...containers.slice(index + 1)])
    setSelected(null)
  }

  const handleContainerClick = index => {
    setSelected(index)
  }

  const onStyleChange = (name, value) => {
    const newContainers = [...containers]
    newContainers[selected].styles = {
      ...containers[selected].styles,
      [name]: value
    }
    setContainers(newContainers)
  }

  const onPropChange = (name, value) => {
    const newContainers = [...containers]
    newContainers[selected].props = {
      ...containers[selected].props,
      [name]: value
    }
    setContainers(newContainers)
  }

  return (
    <React.Fragment>
      <header className="header">
        <div className="container">
          <h1 className="title">Flex Playground</h1>
          <div className="buttons">
            <Button label="Add container" className="primary" handleClick={addContainer} />
          </div>
        </div>
      </header>
      <section className="body container">
        <DrawArea
          containers={containers}
          selected={selected}
          handleContainerClick={handleContainerClick}
        />
        <Controls
          container={selected !== null ? containers[selected] : {}}
          onStyleChange={onStyleChange}
          onPropChange={onPropChange}
          visible={selected !== null}
          removeContainer={() => removeContainer(selected)}
        />
      </section>
      <footer className="footer">
        <div className="container">

        </div>
      </footer>
    </React.Fragment>
  )
}