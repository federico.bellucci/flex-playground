import React from 'react'
import './FlexItem.sass'

const FlexItem = ({ text }) => {
  return (
    <div className="flex-item" >
      {text}
    </div>
  )
}

export default FlexItem