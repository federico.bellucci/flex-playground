import React, { useState } from 'react'
import './Controls.sass'
import Select from './form/Select'
import Input from './form/Input'
import SafeUnsafe from './form/SafeUnsafe'
import Button from './Button'

export default ({ container, visible, onStyleChange, onPropChange, removeContainer }) => {
  const { styles, props } = container

  const onInputChange = e => {
    onStyleChange(e.target.name, e.target.value)
  }

  const onSelectChange = name => e => {
    onStyleChange(name, e.target.value)
  }

  const onSafeChange = (name, value, checked) => {
    let newValue = {
      ...props[name],
      [value]: checked
    }
    if (checked) {
      if (value === 'safe') {
        newValue.unsafe = false
      } else {
        newValue.safe = false
      }
    }
    onPropChange(name, newValue)
  }

  return (
    <div className="controls">
      {visible
        ? <>
          <Button label="Remove container" className="primary" handleClick={removeContainer} />
          <form>
            <Input
              name="width"
              label="width"
              value={styles.width}
              onChange={onInputChange}
            />
            <Input
              name="height"
              label="height"
              value={styles.height}
              onChange={onInputChange}
            />
            <Select
              name="flexDirection"
              label="flex-direction"
              value={styles.flexDirection}
              onChange={onSelectChange("flexDirection")}
              options={['', 'row', 'row-reverse', 'column', 'column-reverse']} />
            <Select
              name="flexWrap"
              label="flex-wrap"
              value={styles.flexWrap}
              onChange={onSelectChange("flexWrap")}
              options={['', 'wrap', 'nowrap', 'wrap-reverse']} />
            <Select
              name="justifyContent"
              label="justify-content"
              value={styles.justifyContent}
              onChange={onSelectChange("justifyContent")}
              options={['', 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly', 'start', 'end', 'left', 'right']} />
            <SafeUnsafe
              name="justifyContentSafe"
              value={props.justifyContentSafe}
              onChange={onSafeChange}
            />
            <Select
              name="alignItems"
              label="align-items"
              value={styles.alignItems}
              onChange={onSelectChange("alignItems")}
              options={['', 'stretch', 'flex-start', 'flex-end', 'center', 'baseline', 'first baseline', 'last baseline', 'start', 'end', 'self-start', 'self-end']} />
            <SafeUnsafe
              name="alignItemsSafe"
              value={props.alignItemsSafe}
              onChange={onSafeChange}
            />
            <Select
              name="alignContent"
              label="align-content"
              value={styles.alignContent}
              onChange={onSelectChange("alignContent")}
              options={['', 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly', 'stretch', 'start', 'end', 'baseline', 'first baseline', 'last baseline']} />
            <SafeUnsafe
              name="alignContentSafe"
              value={props.alignContentSafe}
              onChange={onSafeChange}
            />
          </form>
        </>
        : null}
    </div>
  )
}