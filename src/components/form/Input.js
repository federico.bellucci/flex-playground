import React from 'react'
import './form.sass'

export default ({ value, name, label, type, onChange }) => {
  return (
    <div className="form-control">
      <label className="form-label">
        <span className="label-text">{label}</span>
        <input className="form-input" name={name} type={type} value={value} onChange={onChange} />
      </label>
    </div>
  )
}