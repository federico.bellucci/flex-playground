import React from 'react'
import './form.sass'

export default ({ value, checked, name, label, onChange }) => {
  const onCheck = e =>
    onChange(name, e.target.value, e.target.checked)

  return (
    <label className="form-checkbox">
      <span className="label-checkbox">{label}</span>
      <input
        type="checkbox"
        className="checkbox-input"
        name={name}
        value={value}
        checked={checked}
        onChange={onCheck}
      />
    </label>
  )
}