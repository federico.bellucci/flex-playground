import React from 'react'
import './form.sass'

export default ({ value, name, label, onChange, options }) => {

  return (
    <div className="form-control">
      <label className="form-label">
        <span className="label-text">{label}</span>
        <select className="form-input" name={name} value={value} onChange={onChange} >
          {options.map(item => {
            let label, value
            if (typeof item !== 'object' || item === null) {
              label = value = item
            } else {
              label = item.label
              value = item.value
            }
            return <option key={value} value={value} >{label}</option>
          })}
        </select>
      </label>
    </div>
  )
}