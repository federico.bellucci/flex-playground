import React from 'react'
import Checkbox from './Checkbox'
import './form.sass'

export default ({ value, name, onChange }) => {
  return (
    <div className="form-control checkbox-group">
      <Checkbox
        name={name}
        value="safe"
        label="safe"
        checked={value.safe}
        onChange={onChange}
      />
      <Checkbox
        name={name}
        value="unsafe"
        label="unsafe"
        checked={value.unsafe}
        onChange={onChange}
      />
    </div>
  )
}