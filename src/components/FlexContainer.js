import React from 'react'
import './FlexContainer.sass'

const defaultStyle = {
  display: 'flex'
}

const editStyles = (styles, props) => {
  const newStyles = { ...styles }

  const { justifyContentSafe, alignItemsSafe, alignContentSafe } = props
  if (justifyContentSafe.safe || justifyContentSafe.unsafe) {
    newStyles.justifyContent = `${justifyContentSafe.safe ? 'safe' : 'unsafe'} ${newStyles.justifyContent}`
  }

  if (alignItemsSafe.safe || alignItemsSafe.unsafe) {
    newStyles.alignItems = `${alignItemsSafe.safe ? 'safe' : 'unsafe'} ${newStyles.alignItems}`
  }

  if (alignContentSafe.safe || alignContentSafe.unsafe) {
    newStyles.alignContent = `${alignContentSafe.safe ? 'safe' : 'unsafe'} ${newStyles.alignContent}`
  }

  return newStyles
}

const FlexContainer = ({ styles, props, children, handleClick, selected }) => {
  return (
    <div style={{ ...defaultStyle, ...editStyles(styles, props) }} onClick={handleClick}
      className={(selected ? "selected " : "") + "flex-container"}>
      {children}
    </div>
  )
}

export default FlexContainer