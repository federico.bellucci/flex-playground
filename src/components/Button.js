import React from 'react'
import './Button.sass'

export default ({ label, className, handleClick }) => {
  return (
    <button
      className={(className ? className + " " : "") + "button"}
      onClick={handleClick}
    >
      {label}
    </button>
  )
}