import React from 'react'
import FlexContainer from './FlexContainer'
import './DrawArea.sass'
import FlexItem from './FlexItem'

const generateItems = count => {
  let items = []
  for (let i = 1; i <= count; i++) {
    items.push(<FlexItem text={i} key={i} />)
  }
  return items
}

export default ({ containers, selected, handleContainerClick }) => {
  return (
    <div className="draw-area" onClick={e => handleContainerClick(null)}>
      {containers.map((container, index) =>
        <FlexContainer
          styles={container.styles}
          props={container.props}
          key={index}
          selected={index === selected}
          handleClick={e => {
            handleContainerClick(index);
            e.stopPropagation()
          }} >
          {generateItems(8)}
        </FlexContainer>)}
    </div>
  )
}
